%undefine __cmake_in_source_build

%global xapian_core_support "ON"
%global clang_support OFF
Name:                doxygen
Epoch:               1
Version:             1.13.2
Release:             1
Summary:             A documentation system for C/C++
License:             GPL-1.0-or-later
Url:                 https://www.doxygen.nl
Source0:             https://www.doxygen.nl/files/%{name}-%{version}.src.tar.gz
Source1:             doxywizard.desktop
Source2:             doxywizard-icons.tar.xz

BuildRequires:       python3 ImageMagick gcc-c++ gcc perl-interpreter
BuildRequires:       python3-libxml2 texlive-bibtex sqlite-devel
BuildRequires:       desktop-file-utils graphviz flex bison cmake
%if %{xapian_core_support} == "ON"
BuildRequires:       xapian-core-devel zlib-devel
%endif
%if "x%{?clang_support}" == "xON"
BuildRequires:       llvm-devel
BuildRequires:       clang-devel
%endif

Requires:            perl-interpreter
Requires:            graphviz

%description
Doxygen is the de facto standard tool for generating documentation from
annotated C++ sources, but it also supports other popular programming
languages such as C, Objective-C, C#, PHP, Java, Python, IDL (Corba,
Microsoft, and UNO/OpenOffice flavors), Fortran, VHDL, Tcl, and to some
extent D.

%package doxywizard
Summary:             A GUI for creating and editing configuration files
Requires:            %{name} = %{epoch}:%{version}-%{release}
BuildRequires:       qt6-qtbase-devel
%description doxywizard
Doxywizard is a graphical front-end to read/edit/write doxygen
configuration files.

%prep
%autosetup -p1 -a2
iconv --from=ISO-8859-1 --to=UTF-8 LANGUAGE.HOWTO > LANGUAGE.HOWTO.new
touch -r LANGUAGE.HOWTO LANGUAGE.HOWTO.new
mv LANGUAGE.HOWTO.new LANGUAGE.HOWTO

%build
%cmake \
      -Dbuild_doc=OFF \
      -Dbuild_wizard=ON \
      -Dbuild_xmlparser=ON \
      -Dbuild_search=%{xapian_core_support} \
      -Duse_sys_sqlite3:BOOL=ON \
      -DMAN_INSTALL_DIR=%{_mandir}/man1 \
      -DCMAKE_INSTALL_PREFIX:PATH=%{_prefix} \
      -DBUILD_SHARED_LIBS=OFF \
      -Duse_libclang=%{clang_support} \
%{nil}
%cmake_build

%install
%cmake_install
icondir=%{buildroot}%{_datadir}/icons/hicolor
install -m755 -d $icondir/{16x16,32x32,48x48,128x128}/apps
install -m644 -p -D doxywizard-6.png $icondir/16x16/apps/doxywizard.png
install -m644 -p -D doxywizard-5.png $icondir/32x32/apps/doxywizard.png
install -m644 -p -D doxywizard-4.png $icondir/48x48/apps/doxywizard.png
install -m644 -p -D doxywizard-3.png $icondir/128x128/apps/doxywizard.png
install -d %{buildroot}/%{_mandir}/man1
cp doc/*.1 %{buildroot}/%{_mandir}/man1/
%if %{xapian_core_support} == "OFF"
rm -f %{buildroot}/%{_mandir}/man1/doxyindexer.1* %{buildroot}/%{_mandir}/man1/doxysearch.1*
%endif
desktop-file-install --dir=%{buildroot}%{_datadir}/applications %{S:1}

%check
%ctest

%files
%doc LANGUAGE.HOWTO README.md
%license LICENSE
%{_bindir}/doxygen
%{_mandir}/man1/doxygen.1*
%if %{xapian_core_support} == "ON"
%{_bindir}/{doxyindexer,doxysearch*}
%{_mandir}/man1/{doxyindexer.1*,doxysearch.1*}
%endif

%files doxywizard
%{_bindir}/doxywizard
%{_mandir}/man1/doxywizard*
%{_datadir}/applications/doxywizard.desktop
%{_datadir}/icons/hicolor/*/apps/doxywizard.png

%changelog
* Fri Jan 10 2025 Funda Wang <fundawang@yeah.net> - 1:1.13.2-1
- update to 1.13.2

* Fri Jan 03 2025 Funda Wang <fundawang@yeah.net> - 1:1.13.1-1
- update to 1.13.1

* Wed Jan 01 2025 Funda Wang <fundawang@yeah.net> - 1:1.13.0-2
- backport fix for regression with HTML_DYNAMIC_MENUS = NO

* Sat Dec 28 2024 Funda Wang <fundawang@yeah.net> - 1:1.13.0-1
- update to 1.13.0
- build wizard with qt6, as qt5 was EOLed
- build with system sqlite3

* Mon Nov 11 2024 Funda Wang <fundawang@yeah.net> - 1:1.12.0-2
- adopt to new cmake macro

* Mon Sep 30 2024 Funda Wang <fundawang@yeah.net> - 1:1.12.0-1
- update to 1.12.0

* Thu Mar 21 2024 wangkai <13474090681@163.com> - 1:1.10.0-1
- Update to 1.10.0

* Mon Apr 24 2023 xu_ping <707078654@qq.com> - 1:1.9.6-1
- Upgrade package to 1.9.6 version

* Wed Sep 21 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 1:1.9.3-3
- set clang_support to default OFF

* Mon Aug 15 2022 caodongxia <caodongxia@h-partners.com> - 1:1.9.3-2
- RTFGenerator should exit with 0 when gengrate file success

* Mon Apr 11 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 1:1.9.3-1
- Update to 1.9.3-1

* Tue Mar 29 2022 baizhonggui<baizhonggui@huawei.com> - 1.9.2-2
- Fix build error for string compare

* Thu Dec 30 2021 zoulin<zoulin13@huawei.com> - 1.9.2-1
- Update version to 1.9.2 and removeing the "graphviz" dependency

* Fri Jun 4 2021 wangchen<wangchen137@huawei.com> - 1.8.18-9
- Fix md5 hash does not match for two different runs

* Tue May 11 2021 wangchen<wangchen137@huawei.com> - 1.8.18-8
- RTFGenerator should exit with 0 when gengrate file success

* Tue Jan 05 2021 shixuantong<shixuantong@huawei.com> - 1.8.18-7
- fix incorrect label in map of dot files in xhtml

* Thu Dec 10 2020 shixuantong<shixuantong@huawei.com> - 1.8.18-6
- Modify Source0

* 20201016023007641729 patch-tracking 1.8.18-5
- append patch file of upstream repository from <ef6cc2b1b3b70fba03e994e7a61926c8e2958867> to <ef6cc2b1b3b70fba03e994e7a61926c8e2958867>

* Fri Sep 11 2020 baizhonggui <baizhonggui@huawei.com> - 1.8.18-4
- Modify Source0

* Tue Jun 2 2020 Guoshuai Sun <sunguoshuai@huawei.com> - 1.8.18-3
- Update to 1.8.18

* Tue Dec 10 2019 mengxian <mengxian@huawei.com> - 1:1.8.14-8
- Package init

* Mon May 6 2019 fanmenggang <fanmenggang@huawei.com> - 1:1.8.14-7.h1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Eliminate PDF file Differences
